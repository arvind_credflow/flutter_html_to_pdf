//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<flutter_full_pdf_viewer/FlutterFullPdfViewerPlugin.h>)
#import <flutter_full_pdf_viewer/FlutterFullPdfViewerPlugin.h>
#else
@import flutter_full_pdf_viewer;
#endif

#if __has_include(<flutter_html_to_pdf/FlutterHtmlToPdfPlugin.h>)
#import <flutter_html_to_pdf/FlutterHtmlToPdfPlugin.h>
#else
@import flutter_html_to_pdf;
#endif

#if __has_include(<path_provider/PathProviderPlugin.h>)
#import <path_provider/PathProviderPlugin.h>
#else
@import path_provider;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FlutterFullPdfViewerPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterFullPdfViewerPlugin"]];
  [FlutterHtmlToPdfPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterHtmlToPdfPlugin"]];
  [FLTPathProviderPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTPathProviderPlugin"]];
}

@end
